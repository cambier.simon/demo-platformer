import { Container } from 'pixi.js'
import { App, Tools } from 'squaredjs'

export default class Camera {

  public container?: Container

  public setContainer(container: Container): void {
    this.container = container
  }

  public lookAt(x: number, y: number): void {
    if (this.container) {
      const width = App.instance.width
      const height = App.instance.height
      const center = { x: -x + width / 2, y: -y + height / 2 }
      this.container.position.x = Tools.limit(center.x, -(this.container.width - width), 0)
      this.container.position.y = Tools.limit(center.y, -(this.container.height - height), 0)
    }
  }
}
