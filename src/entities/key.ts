import { Entity, EntitySettings, Spritesheet, Vector2, Constants, Sound, Tools, COLLISION_TYPE } from 'squaredjs'
import * as TWEEN from '@tweenjs/tween.js'
import Game from '../game'
import { Sprite } from 'pixi.js'
const debug = require('debug')('game:entity:key')

export default class Key extends Entity {

  private tweenFloat: TWEEN.Tween
  private soundPickup = new Sound('assets/sounds/key.ogg')

  constructor(settings: EntitySettings) {
    super(settings)

    this.body.gravityFactor = 0
    this.body.collisionType = COLLISION_TYPE.PASSIVE

    const tileset = new Spritesheet('assets/tilesets/platformPack_tilesheet.png', 64, 64)
    this.addAnimation(tileset, 'blue', [14 * 4 + 7], 1)
    this.addAnimation(tileset, 'yellow', [14 * 4 + 8], 1)
    this.addAnimation(tileset, 'green', [14 * 4 + 9], 1)

    this.width = 42
    this.height = 30
    this.offset.x = 10
    this.offset.y = 20

    if (this.properties.color) {
      this.setAnimation(this.properties.color)
    }

    // Looping floating animation
    this.tweenFloat = new TWEEN.Tween({ y: this.y })
      .to({ y: this.y - 30 }, 900 + Math.random() * 200)
      .onUpdate((o: any) => {
        this.y = o.y
      })
      .easing(TWEEN.Easing.Quadratic.InOut)
      .repeat(Infinity)
      .yoyo(true)
      .start()
  }

  public update(delta: number): void {
    super.update(delta)
    if (Tools.isDev()) {
      this.body.drawBox()
    }
  }

  public onCollisionStart(entity: Entity): void {
    this.soundPickup.play()

    // Stop the floating animation
    this.tweenFloat.stop()

    // Get player coordinates relative to the viewport
    const globalPos = {
      x: this.sprite.toGlobal(this.sprite.parent.position).x,
      y: this.sprite.toGlobal(this.sprite.parent.position).y
    }

    // Copy the sprite and place it on the UI
    const sprite = new Sprite(this.sprite.texture.clone())
    Game.instance.ui.addChild(sprite)
    sprite.x = globalPos.x
    sprite.y = globalPos.y

    // Destroy self
    Game.instance.destroyEntity(this)

    // Save the fact that we have a key
    ++Game.instance.nbCollectedKeys

    // Move the key to the corner
    new TWEEN.Tween(globalPos)
      .to({ x: 10, y: 50 + Game.instance.nbCollectedKeys * 50 }, 1000)
      .easing(TWEEN.Easing.Cubic.Out)
      .onUpdate((o: Vector2) => {
        sprite.x = o.x
        sprite.y = o.y
      })
      .start()
  }

}
