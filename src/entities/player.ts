import { Entity, EntitySettings, Spritesheet, App, Sound, Tools, COLLISION_TYPE } from 'squaredjs'
import * as TWEEN from '@tweenjs/tween.js'
import Game from '../game'
const debug = require('debug')('game:entity:player')

class Player extends Entity {

  public dead = false

  private soundJump = new Sound('assets/sounds/jump.flac')

  constructor(settings: EntitySettings) {
    super(settings)

    const tileset = new Spritesheet('assets/spritesheets/platformerPack_character.png', 96, 96)
    this.addAnimation(tileset, 'idle', [0], 1)
    this.addAnimation(tileset, 'jump', [1], 1)
    this.addAnimation(tileset, 'walk', [2, 3], 200, { loop: true })
    this.addAnimation(tileset, 'dying', [4, 5], 100, { loop: true })

    this.offset.x = 20
    this.offset.y = 31

    // Collisions
    this.body.friction.x = Number.MAX_SAFE_INTEGER
    this.body.friction.y = 0
    this.body.gravityFactor = 10
    this.body.maxVel = { x: 512, y: Number.MAX_SAFE_INTEGER }
    this.body.collisionType = COLLISION_TYPE.PASSIVE
  }

  public setVel(x: number, y: number): void {
    this.body.vel.x = x
    this.body.vel.y = y
  }

  public flipX(flip: boolean): void {
    this.flip.x = flip
  }

  public update(delta: number): void {
    super.update(delta)

    if (Tools.isDev()) {
      this.body.drawBox()
    }

    if (this.dead) {
      return
    }

    // Jump
    if (App.instance.input.pressed('jump') && this.body.standing) {
      this.body.vel.y = -1000
      this.soundJump.play()
    }

    // Move left-right
    this.body.accel.x = 0
    const accel = Number.MAX_SAFE_INTEGER
    if (App.instance.input.state('left')) {
      this.body.accel.x = -accel
      this.flipX(true)
    }
    if (App.instance.input.state('right')) {
      this.body.accel.x = accel
      this.flipX(false)
    }

    if (!this.body.standing) {
      this.setAnimation('jump')
    }
    else {
      if (Math.abs(this.body.accel.x)) {
        this.setAnimation('walk')
      }
      else {
        this.setAnimation('idle')
      }
    }
  }

  public freeze(): void {
    this.body.accel.x = 0
    this.body.accel.y = 0
    this.removeAllComponents()
  }

  public kill(): void {
    if (this.dead) { return }
    this.freeze()
    this.setAnimation('dying')
    this.dead = true

    // Death animation
    new TWEEN.Tween(this)
      .to({ y: '-128' }, 1000)
      .easing(TWEEN.Easing.Back.InOut)
      .chain(
        new TWEEN.Tween(this)
          .delay(200)
          .to({ y: '+600' }, 1500)
      )
      .start()

    Game.instance.gameFailure()
  }

}

export default Player
