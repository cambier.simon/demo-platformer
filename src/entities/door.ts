import { Entity, EntitySettings, Spritesheet, Tools, COLLISION_TYPE } from 'squaredjs'
import EntityText from '../components/entityText'
import Game from '../game'
import Player from './player'
const debug = require('debug')('game:entity:door')

export default class Door extends Entity {

  public overText!: EntityText
  private textSuccess = 'Well done!'
  private textMissingKeys = 'You need to bring me\nthe three keys'

  constructor(settings: EntitySettings) {
    super(settings)
    const tileset = new Spritesheet('assets/tilesets/platformPack_tilesheet.png', 64, 128)
    this.addAnimation(tileset, 'closed', [[384, 320], [448, 320], [512, 320]], 1000, { loop: true })
    this.addAnimation(tileset, 'open', [[320, 320]], 1)

    this.body.gravityFactor = 0
    this.body.collisionType = COLLISION_TYPE.PASSIVE

    this.width = 64
    this.height = 128
  }

  public update(delta: number): void {
    super.update(delta)
    if (Tools.isDev()) {
      this.body.drawBox()
    }
  }

  public onCollisionStart(other: Entity): void {
    if (other instanceof Player) {
      if (Game.instance.hasAllKeys) {
        this.overText.text = this.textSuccess
        this.setAnimation('open')
        if (!Game.instance.success) {
          Game.instance.gameCompleted()
        }
      }
      else {
        this.overText.text = this.textMissingKeys
      }
      setTimeout(() => {
        this.overText.label.alpha = 1
      })
    }
  }

  public collisionEnd(other: Entity): void {
    if (other instanceof Player) {
      this.overText.label.alpha = 0
    }
  }

  public onAddedToScene(): void {
    this.overText = new EntityText(this, this.textMissingKeys)
    this.overText.label.alpha = 0
  }
}
