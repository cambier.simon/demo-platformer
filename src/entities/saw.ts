import { Entity, EntitySettings, Spritesheet, Tools, COLLISION_TYPE } from 'squaredjs'
import Player from './player'
const debug = require('debug')('game:entity:saw')

export default class Saw extends Entity {
  constructor(settings: EntitySettings) {
    super(settings)
    const tileset = new Spritesheet('assets/tilesets/platformPack_tilesheet.png', 64, 64)
    this.addAnimation(tileset, 'default', [25], 1)
    this.body.gravityFactor = 0
    this.offset = { x: 16, y: 16 }
    this.body.collisionType = COLLISION_TYPE.PASSIVE

    // Set pivot point to center
    this.pivot = { x: .5, y: .5 }
  }

  public update(delta: number): void {
    super.update(delta)
    if (Tools.isDev()) {
      this.body.drawBox()
    }
    this.sprite.rotation += 20 * delta
  }

  public onCollisionStart(entity: Entity): void {
    if (entity instanceof Player) {
      entity.kill()
    }
  }
}
