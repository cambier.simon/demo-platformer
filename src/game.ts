import * as TWEEN from '@tweenjs/tween.js'
import { App, Entity, Input, UILabel, Sound, RigidBody } from 'squaredjs'
import Camera from './camera'
import Player from './entities/player'

const debug = require('debug')('game:main')

export default class Game extends App {
  public static instance: Game
  public lblPlayerPos = new UILabel()
  public camera = new Camera()

  public nbCollectedKeys = 0
  public get hasAllKeys(): boolean {
    return this.nbCollectedKeys === 3
  }

  public success = false

  private t: number = 0
  private soundSuccess = new Sound('assets/sounds/success.wav')
  private soundFailure = new Sound('assets/sounds/gameover.wav')

  constructor() {
    super('game', { scale: 1, width: 64 * 12, height: 64 * 9 })

    RigidBody.baseGravityValue = 300

    this.assets = require('../assets/assets.json')

    this.input.bind('up', Input.KEY.UP_ARROW)
    this.input.bind('down', Input.KEY.DOWN_ARROW)
    this.input.bind('left', Input.KEY.LEFT_ARROW)
    this.input.bind('right', Input.KEY.RIGHT_ARROW)
    this.input.bind('jump', Input.KEY.SPACE)
  }

  public async init(): Promise<void> {
    const level1 = require('./levels/main.json')
    await game.loadLevel(level1)

    Entity.findByName('player')!.sprite.zOrder = Number.MAX_SAFE_INTEGER

    this.rootContainer.alpha = 1
    this.nbCollectedKeys = 0
    this.success = false

    this.ui.removeChildren()
    this.ui.addChild(this.lblPlayerPos)
    this.lblPlayerPos.x = 10
    this.lblPlayerPos.y = 10
    this.lblPlayerPos.setStyle({
      fontSize: 48,
      fill: 'black',
      strokeThickness: 0
    })

    this.camera.setContainer(this.level)
  }

  public resetGame(): void {
    new TWEEN.Tween({ alpha: Game.instance.rootContainer.alpha })
      .to({ alpha: 0 }, 1500)
      .delay(1000)
      .onUpdate(o => {
        Game.instance.rootContainer.alpha = o.alpha
      })
      .onComplete(async () => {
        await this.init()
      })
      .start()
  }

  public gameFailure(): void {
    this.soundFailure.play()
    this.resetGame()
  }

  public gameCompleted(): void {
    this.success = true;
    (Entity.findByName('player') as Player).freeze()
    this.soundSuccess.play()
    this.resetGame()
  }

  public update(delta: number): void {
    super.update(delta)
    const player = Entity.findByName('player') as Player
    if (player) {
      this.lblPlayerPos.text = `Player pos: ${player.getPos().x}, ${player.getPos().y}`
      if (!player.dead) {
        this.camera.lookAt(player.x, player.y)
      }
    }

    TWEEN.update()
  }
}

const game = new Game()

game.prepare()
  .then(async () => {
    debug('Game loaded')
    await game.init()

    // game.lblDemo = new UILabel('[SquaredJS intensifies]', 20, 32, game.level)
  })
