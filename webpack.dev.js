const merge = require('webpack-merge');
const common = require('./webpack.config.js');
const webpack = require('webpack');

module.exports = merge(common, {
  mode: "development",
  devtool: "source-map",

  plugins: [
    new webpack.EnvironmentPlugin({
      DEBUG: "game:* squared:*"
    }),
  ]
});