const CopyWebpackPlugin = require('copy-webpack-plugin');
const Path = require('path');

const distFolder = 'dist'
const assetsFolder = 'assets'

module.exports = {

  entry: {
    game: './src/game.ts'
  },

  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.tsx?$/,
        loaders: [/*'babel-loader',*/ 'ts-loader']
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.ts'],
    alias: {
      '~': Path.resolve(__dirname, 'src'),
    }
  },

  output: {
    filename: '[name].bundle.js',
    path: Path.resolve(__dirname, distFolder)
  },

  plugins: [

    new CopyWebpackPlugin([
      {
        from: Path.resolve(__dirname, assetsFolder),
        to: Path.resolve(__dirname, distFolder, assetsFolder)
      },
      {
        from: Path.resolve(__dirname, 'index.html')
      },
    ],
      {
        ignore: [],

        // By default, we only copy modified files during
        // a watch or webpack-dev-server build. Setting this
        // to `true` copies all files.
        copyUnmodified: false
      })
  ]
}